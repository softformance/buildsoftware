<?php get_header(); ?>

<!-- TODO: meta author image, batch nav -->

<div id="page-subheader">

	<div class="block-inner">
		<div class="header-container">
			<h1>Get Exclusive Software Business Tips</h1>
			<div class="block-inner-desc">
				<figure>
					<img src="https://chrysmedia.com/smos/wp-content/uploads/2019/01/Ethan-Sigmon.png" />				 </figure>
				<figure>
    				<img src="http://buildsoftware.io/wp-content/uploads/2020/03/vitaliy_circle.png" />
				 </figure>
				<p>Receive the same tips we used to build 100+ tech startups for our clients</p>
			</div>
			<div class="sub-form">
				<form action="https://www.getdrip.com/forms/187618698/submissions" method="post" data-drip-embedded-form="187618698">
					<div>
						<input type="email" id="drip-email" name="fields[email]" value="" placeholder="Email Address">
					</div>
					<div><input type="submit" value="Sign Up" data-drip-attribute="sign-up-button">
				    </div>
					<div style="display: none;" aria-hidden="true">
						<input type="text" id="website" name="website" tabindex="-1" autocomplete="false" value="">
					</div>
				</form>
			</div>
		</div>
	</div>
		
</div>

<div id="main-content" class="blog-template">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
		<?php
			
			// added by Vitaliy on Feb 21, 2020
			$bs_query = new WP_Query(array('post_type'=>'post',
			    'post_status'=>'publish', 'posts_per_page'=>5));
			
			if ( $bs_query->have_posts() ) :
				while ( $bs_query->have_posts() ) : $bs_query->the_post();
					$post_format = et_pb_post_format(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

                <?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) : ?>
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<?php endif; ?>

					<?php
						et_divi_post_meta();
					?>
				<?php endif; ?>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height    = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$alttext   = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
					$thumbnail = get_thumbnail( $width, $height, $classtext, $alttext, $titletext, false, 'Blogimage' );
					$thumb     = $thumbnail["thumb"];

					et_divi_post_format_content();

					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
							printf(
								'<div class="et_main_video_container">
									%1$s
								</div>',
								et_core_esc_previously( $first_video )
							);
						elseif ( ! in_array( $post_format, array( 'gallery' ) ) && 'on' === et_get_option( 'divi_thumbnails_index', 'on' ) && '' !== $thumb ) : ?>
							<a class="entry-featured-image-url" href="<?php the_permalink(); ?>">
								<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
							</a>
					<?php
						elseif ( 'gallery' === $post_format ) :
							et_pb_gallery_images();
						endif;
					} ?>

				<?php if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) : ?>
					<?php

						if ( 'on' !== et_get_option( 'divi_blog_style', 'false' ) || ( is_search() && ( 'on' === get_post_meta( get_the_ID(), '_et_pb_use_builder', true ) ) ) ) {
							truncate_post( 270 );
						} else {
							the_content();
						}
					?>
				<?php endif; ?>

                <a href="<?php the_permalink(); ?>" title="Continue Reading" class="button-more">Continue Reading</a>

					</article> <!-- .et_pb_post -->
			<?php
					endwhile;

					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
						
				else :
					get_template_part( 'includes/no-results', 'index' );
				endif;
			?>
			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
